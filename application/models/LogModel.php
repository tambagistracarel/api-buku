<?php

class LogModel extends CI_Model{

    function checkUser($username,$password) {        
        $this->db->select("*");
        $this->db->from("user");
        $this->db->where("username_user",$username);
        $user = $this->db->get()->row();
        if($user!=null){
            if(password_verify($password,$user->password_user)){
                return $user;
            }
        }
        return null;
    }
    //EXPIRED
    //1. Expired token tidak diupdate jika belum kadaluarsa
    //2. Setiap login langsung update tanggal expired dan tokennya
    function updateExpiredAndTokenUser($id,$data) {
        $this->db->where("id_user",$id);
        return $this->db->update("user",$data);
    }
    //TOKEN
    //1. Untuk setiap login token diupdate
    //2. Hanya mengupdate token jika tokennya kadaluarsa
}
