<?php

require 'CrudFunction.php';

class BukuModel extends CI_Model implements CrudFunction {

    var $table = "buku";
    var $primaryKey = "id_buku";

    public function getAll() {
        return $this->db->get($this->table)->result();
    }

    public function getByPrimaryKey($primaryKey) {
        $this->db->where($this->primaryKey, $primaryKey);
        return $this->db->get($this->table)->row();
    }

    public function insert($data) {
        return $this->db->insert($this->table, $data);
    }

    public function update($data, $primaryKey) {
        $this->db->where($this->primaryKey, $primaryKey);
        return $this->db->update($this->table, $data);
    }

    public function delete($primaryKey) {
        $this->db->where($this->primaryKey, $primaryKey);
        return $this->db->delete($this->table);
    }

    public function totalRow($search = null) {
        $this->db->like("judul_buku", $search);
        $this->db->like("pencipta_buku", $search);
        $this->db->like("description", $search);
        $this->db->or_like("harga", $search);
        $this->db->or_like("stock", $search);
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    public function getLimitData($limit, $start = 0, $search = null) {
        $this->db->like("judul_buku", $search);
        $this->db->like("pencipta_buku", $search);
        $this->db->like("description", $search);
        $this->db->or_like("harga", $search);
        $this->db->or_like("stock", $search);
        $this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

}
