<?php

function checkToken() {
    $ci = &get_instance();
    $token = $ci->input->get_request_header("token");
    //cek token
    $query = "select token_user, token_expired_user from user where token_user='$token'";
    $user = $ci->db->query($query)->row();
    $hariIni = date("Y-m-d");
    //cek apakah token sudah expired
    if ($user != null) {
        if ($hariIni > $user->token_expired_user) {
            //token sudah expired
            return false;
        }
    } else {
        //token yang diinputkan tidak ada di dalam database
        //user==null
        return false;
    }
    return true;
}
    function getLastNomor($table) {
        $CI = &get_instance();
        $query = "select max(nomor) as nomor from $table where year(tanggal_transaksi) = year(now()) ";
        $query .= "AND month(tanggal_transaksi) = month(now()) ";
        $nomor = $CI->db->query($query)->row();
        return $nomor;
    }

    function autoCreate($prefix, $delimeter, $nomor) {
        $s = "";
        foreach ($prefix as $value) {
            $s .= $value . $delimeter;
        }
        return $s . date("Y")
                . $delimeter
                . date("m")
                . $delimeter
                . str_pad($nomor, 4, "0", STR_PAD_LEFT);
    }


