<?php

class Log extends Restserver\Libraries\REST_Controller{
    function __construct() {
        parent::__construct();
        $this->load->model("LogModel");  
    }
    
    function index_post(){
        $username = $this->input->server("PHP_AUTH_USER");
        $password = $this->input->server("PHP_AUTH_PW");
        $user = $this->LogModel->checkUser($username,$password);
        if($user != null){
            //1. Update token dan tanggal expired dari token
            $token = md5($user->username_user.date("d M Y H:i:s"));
            $tokenExpired = date('Y-m-d', strtotime(date("Y-m-d")."+3 days"));
            $dataToken = array(
                "token_user" => $token,
                "token_expired_user" => $tokenExpired
            );
            $this->LogModel->updateExpiredAndTokenUser($user->id_user,$dataToken);
            //Data yang dikembalikan ke client
            //Ambil data yang sudah diubah
            $data = array(
                "username" => $user->username_user,
                "token" => $token,
                "token_expired" => $tokenExpired
            );
            $this->response($data,200);
        }else{
            $pesan = array(
              "message" => "Login gagal, username atau password tidak ditemukan"
            );
            $this->response($pesan,401);
        }
    }
}
