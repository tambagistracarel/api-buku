<?php

if (!defined('BASEPATH'))
    exit('No direct access allowed');

class Buku extends Restserver\Libraries\REST_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("BukuModel");
        header("Content-Type=application/json");
        if (checkToken() == false) {
            $this->response(array("pesan" => "Silahkan Login Dahulu"), 401);
            exit();
        }
    }

    public function index_get($id = null) {
        if ($id == null) {
            $perPage = ($this->get("per_page") == null) ? "10" : $this->get("per_page");
            $page = ($this->get("page") == null) ? "1" : $this->get("page");
            $search = ($this->get("search") == null) ? "" : $this->get("search");
            $start = ((int) $page - 1) * (int) $perPage;
            $total_row = $this->BukuModel->totalRow($search);
            $total_page = ceil($total_row / $perPage) + 1;
            $books = $this->BukuModel->getLimitData($perPage, $start, $search);
            $dataBuku = array();
            foreach ($books as $buku) {
                $buku->image_url = base_url() . "image/" . $buku->gambar;
                $dataBuku[] = $buku;
            }
            $data = array(
                "meta" => array(
                    "search" => $search,
                    "page" => $page,
                    "per_page" => $perPage,
                    "total_row" => $total_row,
                    "total_page" => $total_page
                ),
                "buku_data" => $dataBuku,
            );
            $this->response($data, 200);
        } else {
            $buku = $this->BukuModel->getByPrimaryKey($id);
            if ($buku == null) {
                $this->response(array("message" => "Data tidak ditemukan", 400));
            } else {
                $buku->image_url = base_url() . "image/" . $buku->gambar;
                $this->response($buku, 200);
            }
        }
    }

    public function index_post() {
        //proses tambah image
        $stringBase64 = $this->input->post("gambar", true);
        $fileName = md5(date("d-m-Y H:i:s") . rand(1, 100000));
        $fileName .= ".jpg";
        $decode = base64_decode($stringBase64);
        file_put_contents("image/$fileName", $decode);
        //menambah buku
        $data = array(
            "gambar" => $fileName,
            "judul_buku" => $this->input->post("judul_buku", TRUE),
            "pencipta_buku" => $this->input->post("pencipta_buku", TRUE),
            "description" => $this->input->post("description", TRUE),
            "harga" => $this->input->post("harga", TRUE),
            "stock" => $this->input->post("stock", TRUE),
        );
        echo json_encode($data, 200);
        $result = $this->BukuModel->insert($data);
        if ($result) {
            $pesan = array(
                "message" => "Data Buku Berhasil Disimpan"
            );
            $this->response($pesan, Restserver\Libraries\REST_Controller::HTTP_CREATED);
        } else {
            $pesan = array(
                "message" => "Data Buku Gagal Disimpan"
            );
            $this->response($pesan, Restserver\Libraries\REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function index_put() {
        $buku = json_decode(file_get_contents("php://input"));
        if (file_exists("image/$buku->gambar")) {
            unlink("image/$buku->gambar");
        }
        $stringBase64 = $buku->gambar;
        $fileName = md5(date("d-m-Y H:i:s") . rand(1, 100000));
        $fileName .= ".jpg";
        $decode = base64_decode($stringBase64);
        file_put_contents("image/$fileName", $decode);
        $data = array(
            "gambar" => $fileName,
            "judul_buku" => $buku->judul_buku,
            "pencipta_buku" => $buku->pencipta_buku,
            "description" => $buku->description,
            "harga" => $buku->harga,
            "stock" => $buku->stock
        );
        $result = $this->BukuModel->update($data, $buku->id_buku);
        if ($result) {
            $pesan = array(
                'message' => "Data Buku Berhasil Diubah"
            );
            $this->response($pesan, Restserver\Libraries\REST_Controller::HTTP_OK);
        } else {
            $pesan = array(
                'message' => "Data Buku Gagal Diubah"
            );
            $this->response($pesan, Restserver\Libraries\REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function index_delete($idBuku) {
        $buku = $this->BukuModel->getByPrimaryKey($idBuku);
        if (file_exists("image/$buku->gambar")) {
            unlink("image/$buku->gambar");
        }
        $result = $this->BukuModel->delete($idBuku);
        if ($result) {
            $pesan = array(
                'message' => "Data Buku Berhasil Dihapus"
            );
            $this->response($pesan, Restserver\Libraries\REST_Controller::HTTP_OK);
        } else {
            $pesan = array(
                'message' => "Data Buku Gagal Dihapus"
            );
            $this->response($pesan, Restserver\Libraries\REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

}
