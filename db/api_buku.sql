create database api_buku;
use api_buku;
create table buku(
    id_buku int not null primary key auto_increment,
    gambar varchar(200),    
    judul_buku varchar (100),
    pencipta_buku varchar(100),
    description varchar (1000),
    harga int,
    stock int,
    create_at TIMESTAMP NOT NULL DEFAULT NOW(),
    update_at TIMESTAMP NOT NULL DEFAULT NOW() ON UPDATE now()
);

create table user(
    id_user int not null primary key auto_increment,
    username_user varchar(100) not null,
    password_user varchar(100) not null,
    token_user varchar(100),
    token_expired_user date,
    create_at TIMESTAMP NOT NULL DEFAULT NOW(),
    update_at TIMESTAMP NOT NULL DEFAULT NOW() ON UPDATE now()
);

create table transaksi(
    id_transaksi int not null primary key auto_increment,
    no_transaksi varchar(100),
    tanggal_transaksi date,
    nomor int(4)
);

create TABLE item_transaksi(
    id_item_transaksi int not null primary key auto_increment,
    buku_id_buku int not null,
    transaksi_id_transaksi int not null,
    total_item_transaksi int,
    harga_item_transaksi int,
    ADD FOREIGN KEY (buku_id_buku) REFERENCES buku(id_buku),
    ADD FOREIGN KEY (transaksi_id_transaksi) REFERENCES transaksi(id_transaksi)
);

ALTER TABLE item_transaksi ADD FOREIGN KEY (transaksi_id_transaksi) REFERENCES transaksi(id_transaksi);
ALTER TABLE item_transaksi ADD FOREIGN KEY (buku_id_buku) REFERENCES buku(id_buku);

insert into user(username_user, password_user, token_user, token_expired_user)
values ("admin","$2y$10$ArLZSTGjgsw6fGS/Xks9Zu48UgOzX8Y/KIGJuHn45ufBPMFrwgvEa",md5("as"),"2019-09-04");